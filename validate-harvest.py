#! python3
#! /usr/bin/env python3 -i

"""========================================================================="""
# ValidateHarvest.py
# Description: Validate TGS Steele Street Harvest File
# Maintained By: Joseph Carpinelli
# Last Updated: May 24, 2019

################################### Todo ######################################
### Issues ###
# Items left in column C are being duplicated in file due to not being erased
### Add ###
## Variables ##
# CONSTANTS.Name
## Functionality ##
# Detect if harvest' is different case or misspelled path
# Get arguments from sys.argv[] && validate
# Generic countErrors that takes a lambda that returns a bool (for each error)
### Change ###
## Use-Of ##
# Fix info and debug use
# Use traceback, come-on, man!
## Style ##
# Decapitalize any non-constants
# Return None on void functions / fail
# Comments way off
# Newline between if and else
# Comments ending code blocks
##### If ####
#### MOVE Sections to Separate Files ####
#### Create new sheet for validated ####
### Add ###
# Merged cells above data with borders
# Worksheet.active_cell = "A3"
##### Excel Validation Translation #####
## General ##
# Strip Cells
# Pull cell ranges from named ranges
## Weights ##
# isEven
# is greater than 1400
# is numeric
# RFIDs
# regex 1A4000(21266EDF4000) [lic: 403R-00020]
# isAlphaNum
# len == 24
# Check for duplicate values
## if rooms are "Not Found" ##
## if strain are "Not Found" ##
#################################### Todo #####################################
# ADD adjacent change function for rooms and strains
# 
# Install autopep8 just in case
"""========================================================================="""

"""############################ Start Program ##############################"""
""" Imports """
# Debugging and Logging
import traceback
import time
import logging
import sys

# Utilities
import datetime
import shutil
import os
import re
import pprint
import math
from copy import copy
from operator import itemgetter

# Excel File Manipulation
import openpyxl
from openpyxl.utils import get_column_letter
""" End Imports """


""" Argument Validation """
### TODO ###
""" End Argument Validation """


""" Constants """
# Regular Expressions
Room_Regex = re.compile(r"([A-S])-(\d{1,2})")

# File Extensions
Excel_Extension = ".xlsx"

### OpenPyXL ###
# Sheetnames
l_Weights = "Weights"
l_Report = "Report"

# Column Indices
RFID_Index = 0
Weight_Index = 1
Strain_Index = 2
Room_Index = 3

# Data Ranges
Start_Row = 3
End_Row = 1000
Start_Column = RFID_Index + 1
End_Column = Room_Index + 1

# Column Ranges
RFID = "A3:A1000"
WEIGHT = "B3:B1000"
STRAIN = "C3:C1000"
ROOM = "D3:D1000"

# Sheet Ranges
Weights_Range = "$A$3:$D$1000"

# Validation
RFID_Length = 24
RFID_Beginning = "(1A4000)(21266EDF4000)(XXXXXX)"
### End OpenPyXL ###

### Path ###
# Directories
Working_Directory = os.path.abspath(os.path.dirname(__file__))

# Files
Tmp_File = os.path.join(Working_Directory, "tmp.xlsx")
Deletable_File = os.path.join(Working_Directory, "Safe_to_DELETE_ME.txt")
Log_File = os.path.join(Working_Directory, "debug.log")
# Validated_File_Path = out_filepath[:-5] + "_Validated.xlsx"

# Formatting
Log_Format = "%(asctime)s - %(levelname)s - %(message)s"
### End Path ###
""" End Constants """


""" Logger Setup """
# Setup Logger
try:
  logger = logging.getLogger(__name__) # Get logger with module name

  file_handler = logging.FileHandler(Log_File) # Get file handler
  stream_handler = logging.StreamHandler() # Get stream handler

  formatter = logging.Formatter(Log_Format) # Get formatter

  # Handle all message levels
  logger.setLevel(logging.DEBUG)
  file_handler.setLevel(logging.DEBUG)
  stream_handler.setLevel(logging.INFO)

  # Set formatter
  file_handler.setFormatter(formatter)
  stream_handler.setFormatter(formatter)

  # Add file handlers
  logger.addHandler(file_handler)
  logger.addHandler(stream_handler)

except:
  logger.critical("Exception!")
  logger.critical(str(traceback.format_exc()))
  # Attempt to log setup failure
  logger.warn("Logger setup failed!")
  logger.warn(str(traceback.format_exc()))

  # Notify setup failure outside logging
  print("An exception occured... Logging disabled.")

  logging.disable()  # Disable logging after failed setup
""" End Logger Setup """


""" Functions """
# Returns the date formatted as mm-dd
def getFormattedDate(in_Leading_Zero = True):
  logger.info("Getting today's date...")

  Date = str(datetime.datetime.now().date()) # gets yyyy-mm-dd string
  out_date = Date[-5:] # gets mm-dd from Date

  if not in_Leading_Zero:
    logger.info("Removing leading zero from month...")
    out_date = out_date[1:]
  
  return out_date

# Converts in_Sheetlist None values to empty string
def convertNoneToString(in_sheetlist):
  for row in in_sheetlist:
    for cell_value in row:
      if cell_value is None:
        cell_value = str()

# Takes a filepath and checks the parent directory for the file
# Returns the path if found
# Returns None if the file is not found
# LOGGER FOR FILE PATH
def getParentFilepath(in_filepath):
  basename = os.path.basename(in_filepath)  # get just filename
  directoryname = os.path.dirname(in_filepath)  # get just the directoryname

  # Check for same file in the parent directory
  logger.info("Checking parent directory for %s" %(basename))
  out_filepath = os.path.abspath(os.path.join(directoryname, os.pardir))
  out_filepath = os.path.join(out_filepath, basename)

  # If no file found, return None
  if not os.path.exists(out_filepath):
    logger.info("File %s not found..." %(out_filepath))
    out_filepath = None

  return out_filepath

# Gets filename from the user and checks if it exists
# Returns a string containing the path of the filename
def getFilepath():
  logger.info("Taking user input...")

  try:
    in_filename = input("Enter the excel file's name: ")

  except:
    # Log input failure
    logger.critical("Failed to receive or parse input!")
    logger.critical(str(traceback.format_exc()))
    logger.critical("Exiting program in 5 seconds...\n")
    time.sleep(5)
    sys.exit(-1)

  # Detect if extension is missing and add if so
  logger.debug("Checking if Excel extension was provided...")
  if in_filename[-5:] != Excel_Extension:
    logger.info("Adding Excel file extension: %s" %(Excel_Extension))
    in_filename += Excel_Extension

  out_filepath = os.path.join(Working_Directory, in_filename)

  # If file does not exist

  logger.info("Checking if %s exists..." %(os.path.basename(out_filepath)))

  if not os.path.exists(out_filepath):
    logger.info("File %s not found..." %(out_filepath))
    out_filepath = getParentFilepath(out_filepath)

  return out_filepath

# Returns a filepath based on today's date
def getDetectedFilepath(in_Leading_Zeros = True):
  date = getFormattedDate(in_Leading_Zeros)

  Detected_Basename = "Harvest " + date + Excel_Extension

  logger.info("Getting path for %s..." %(Detected_Basename))
  out_detected_filepath = os.path.join(Working_Directory, Detected_Basename)

  logger.info("Checking if %s exists..." %(out_detected_filepath))
  if not os.path.exists(out_detected_filepath):
    logger.info("File %s not found..." %(out_detected_filepath))
    out_detected_filepath = getParentFilepath(out_detected_filepath)

  return out_detected_filepath

# Takes and openpyxl workbook and sheetname
# Returns a worksheet as a list
def getSheetlist(in_Workbook, in_Sheetname):
  Sheet = in_Workbook[in_Sheetname]

  raw_rows = list()  # create a list at function scope
  out_filtered_rows = list()  # create a sorted list at function scope

  for row in Sheet.iter_rows(min_row=Start_Row,
      max_row=End_Row,
      max_col=End_Column,
      values_only=True):
    logger.debug("Adding raw row %s" % (str(row)))
    raw_rows.append(row)

  # Remove None values from the list
  for sublist in raw_rows:
    # Remove None values from sublist
    tmp_filtered_rows = [row for row in sublist if row is not None]

    # Check if any values are left
    if (len(tmp_filtered_rows) > 0) and ((sublist[0] != None) and
        (sublist[1] != None)):
      logger.info("Adding filtered row: %s" % (str(sublist)))
      out_filtered_rows.append(sublist)  # append if list is left

  return out_filtered_rows

# Takes an openpyxl Workbook file and cell location information
# Capitalizes the range of cells specified in the argument parameters
# Returns a string containing the path to the capitalized file
def getUppercaseSheetlist(in_Sheetlist):
  logger.critical("Row len (uppercase before): %s" %(str(len(in_Sheetlist))))
  out_sheetlist = list()  # create return variable at scope

  for row_number, row in enumerate(in_Sheetlist, start=1):
    uppercase_row = list()  # list to hold capitalized rows before appending

    for column_number, cell_value in enumerate(row, start=1):
      if cell_value is None:
        cell_value = ""

      # capitalize all characters
      logger.info("Changing %s to uppercase..." 
          %(get_column_letter(column_number) + str(row_number)))
      uppercase_row.append(str(cell_value).upper())

      logger.info("Changed %s to %s"
          % (get_column_letter(column_number) +
            str(row_number),
            uppercase_row[column_number - 1]))

    out_sheetlist.append(uppercase_row)
   
  logger.critical("Row len (uppercase after): %s" %(str(len(out_sheetlist))))

  return out_sheetlist

# Takes a row of text from getSheetlist
# Returns a list containing the letter and number of the room
# If match is not found
# Returns a list containing an empty string and then a 0
def getRoomGroups(in_row):
  regex_match = Room_Regex.search(str(in_row[Room_Index]))

  if regex_match is not None:
    return [regex_match.group(1), int(regex_match.group(2))]

  else:
    return ["", 0]


# Takes a list containing the letter and then number of a room
# Returns the letter
def f_getRoomLetter(in_room_split): return getRoomGroups(in_room_split)[0]

# Takes a list containing the letter and then number of a room
# Returns the number
def f_getRoomNumber(in_room_split): return getRoomGroups(in_room_split)[1]

# Returns a sorted 2D list
def getSortedSheetlist(in_row_list):
  logger.info("Sorting by RFID as tertiary sort...")

  # tertiary sort by RFID, as new list
  out_sorted_rows = sorted(in_row_list, key=itemgetter(RFID_Index))

  # Sorting by room

  logger.info("Sorting by room...")

  # secondary sort, reversed
  logger.info("Sorting by room number as secondary sort...")
  out_sorted_rows.sort(key=f_getRoomNumber)

  # primary sort by room letter
  logger.info("Sorting by room letter as primary sort...")
  out_sorted_rows.sort(key=f_getRoomLetter)

  return out_sorted_rows

# Takes a Workbook, a Sheetname, and a 2D list to insert
# Saves 2D as an Excel Spreadsheet
# Returns None if succesful, if fail, throws exception and exits
def saveListAs(in_workbook, in_Sheetname, in_rows, in_Validated_File_Path):
  Sheet = in_workbook[in_Sheetname]
  """
  logger.info("Getting column styles...")
  Column_Styles = [copy(Sheet[RFID[:2]].style),
                   copy(Sheet[WEIGHT[:2]].style),
                   copy(Sheet[CORRECTED[:2]].style),
                   copy(Sheet[STRAIN[:2]].style),
                   copy(Sheet[ROOM[:2]].style)]
  """
  # Clears column C
  #Sheet.delete_cols(Corrected_Index + 1)

  # Set each item to corresponding cell
  for row_number, row in enumerate(in_rows, start=Start_Row):
    for column_number, cell_value in enumerate(row, start=1):
      cell_coordinate = get_column_letter(column_number)
      cell_coordinate += str(row_number)
      cell = Sheet[cell_coordinate]

      """
      # Get copies of cell styles
      if cell.has_style:
          cell_font = copy(cell.font)
          cell_border = copy(cell.border)
          cell_fill = copy(cell.fill)
          cell_number_format = copy(cell.number_format)
          cell_protection = copy(cell.protection)
          cell_alignment = copy(cell.alignment)
      """

      # skip empty values
      if (cell_value == "") or (cell_value is None):
        continue

      # convert value to proper data type
      if column_number == 2:
        Sheet[cell_coordinate].value = float(cell_value)

      else:
        Sheet[cell_coordinate].value = cell_value

      """
      # Set cell styles
      if cell.has_style:
          cell.font = cell_font
          cell.border = cell_border
          cell.fill = cell_fill
          cell.number_format = cell_number_format
          cell.protection = cell_protection
          cell.alignment = cell_alignment
      """

      logger.info("Changed %s to %s" %
          (cell_coordinate, str(cell.value)))
    # End for
  # End for

  # Clear cells below data

  data_end = len(in_rows) + Start_Row

  logger.info("Clearing original worksheet from row %s to row %s"
    % (data_end, End_Row))
  Sheet.delete_rows(data_end, End_Row - data_end)

  # Save Validated Workbook

  logger.info("Saving validated spreadsheet...")

  try:
    in_workbook.save(in_Validated_File_Path)

  except:
    logger.critical("Exception!")
    logger.critical(str(traceback.format_exc()))
    logger.critical("Unable to save file!")
    logger.critical("The filename attempted was %s" %
        (in_Validated_File_Path))
    logger.critical("Exiting program in 5 seconds...\n")
    time.sleep(5)
    sys.exit(-1)

  logger.info("Saved at %s" % (in_Validated_File_Path))

  return None
""" End Functions """


""" Main """
def main():
  logger.info("Started program...")

  # Get Filename

  read_filepath = getFilepath()

  # Check if the file exists
  if read_filepath is None:
    logger.warn("Error! The filename provided was not found...")
    logger.warn("Attempting to detect file automatically...")

    read_filepath = getDetectedFilepath()

    # Try without leading zeros
    if read_filepath is None:
      read_filepath = getDetectedFilepath(in_Leading_Zeros = False)

  # Check if the file exists again
  if read_filepath is None:
    logger.critical("Error! Could not detect a file using today's date...")
    logger.critical("Try running again with a different name")
    logger.critical("Exiting program in 5 seconds...")
    time.sleep(5)
    sys.exit(-1)

  Validated_File_Path = read_filepath[:-5] + "_Validated.xlsx"

  # Load Workbook

  logger.info("Loading workbook %s..." % (read_filepath))
  try:
    spreadsheet = openpyxl.load_workbook(read_filepath, data_only=True)

  except:
    logger.critical("Exception!")
    logger.critical(str(traceback.format_exc()))
    logger.critical("Unable to load excel file!")
    logger.critical("The filename used was %s" % (read_filepath))
    logger.critical("Exiting program in 5 seconds...\n")
    time.sleep(5)
    sys.exit(-1)

  logger.info("Workbook successfully loaded")

  # Convert worksheet into 2D list
  sheetlist = getSheetlist(spreadsheet, l_Weights)

  # Change sheet to uppercase
  capitalized_rows = getUppercaseSheetlist(sheetlist)

  # Sort
  validated_rows = getSortedSheetlist(capitalized_rows)

  """
  # Print
  print("Here is the validated list:")
  pprint.pprint(validated_rows)
  print('\n')
  """

  # Saving Validated Spreadsheet
  saveListAs(spreadsheet, l_Weights, validated_rows, Validated_File_Path)

  logger.info("Exiting program in 5 seconds...\n")
  time.sleep(5)
""" End Main """


""" Call main() """
if __name__ == "__main__":
  main()
""" End Call main() """
"""############################## End Program ##############################"""

